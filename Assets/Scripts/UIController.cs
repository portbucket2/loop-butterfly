using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
//using UnityEngine.SceneManagement;
using DG.Tweening;

public class UIController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI blueBF;
    [SerializeField] TextMeshProUGUI purpleBF;
    [SerializeField] TextMeshProUGUI orangeBF;

    [SerializeField] GameObject levelCopletePanel;
    [SerializeField] Button nextLevelButton;

    [SerializeField] TextMeshProUGUI comboText;
    [SerializeField] GameObject tutorialText;

    int blues = 0;
    int purples = 0;
    int oranges = 0;

    GameController gameController;
    GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.GetManager();
        gameController = GameController.GetController;
        blueBF.text = "0";    
        purpleBF.text = "0";    
        orangeBF.text = "0";

        nextLevelButton.onClick.AddListener(delegate {
            //SceneManager.LoadScene(0);
            gameManager.GotoNextStage();
	    });

        if (gameManager.IsGameOpeningLevel())
            tutorialText.SetActive(true);
        else
            tutorialText.SetActive(false);
    }

    
    public void UpdateUI(int _blue, int _purple, int _orange )
    {
        blueBF.text = "" + _blue + " /" + blues;
        purpleBF.text = "" + _purple + " /" + purples;
        orangeBF.text = "" + _orange + " /" + oranges;
        gameController.CheckCompletion(_blue, _purple, _orange);
    }
    public void LevelComplete()
    {
        levelCopletePanel.SetActive(true);
    }

    public void SetTargetValues(int _blue, int _purple, int _orange)
    {
        blues = _blue;
        purples = _purple;
        oranges = _orange;
        UpdateUI(0, 0, 0);
    }
    public void ShowComboText(int count)
    {
        if (count < 2)
            return;

        comboText.text = count + " X Combo";
        comboText.transform.DOPunchScale(Vector3.one, 2f, 1, 1).OnComplete(()=> {
            comboText.transform.DOScale(Vector3.zero, 0.1f);
        });
    }
}
