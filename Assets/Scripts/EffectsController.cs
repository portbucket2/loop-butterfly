using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EffectsController : MonoBehaviour
{
    [SerializeField] int poolCount = 5;
    [Header("prefabs :")]
    [SerializeField] GameObject puff;
    [SerializeField] GameObject dust;
    [SerializeField] GameObject end;

    [SerializeField] Transform endPoint;
    [SerializeField] int effectCount = 0;

    [Header("Debug :")]
    [SerializeField] List<ParticleSystem> puffs;
    [SerializeField] List<ParticleSystem> dusts;
    [SerializeField] ParticleSystem ends;

    void Start()
    {
        puffs = new List<ParticleSystem>();
        dusts = new List<ParticleSystem>();
        //ends = new List<ParticleSystem>();
        for (int i = 0; i < poolCount; i++)
        {
            GameObject pp = Instantiate(puff);
            puffs.Add(pp.GetComponent<ParticleSystem>());

            GameObject dd = Instantiate(dust);
            dusts.Add(dd.GetComponent<ParticleSystem>());

        }
        GameObject ee = Instantiate(end);
        ends = ee.GetComponent<ParticleSystem>();
        ends.transform.position = endPoint.position;
    }

    public void ConsumeButterfly(Transform pos)
    {
        if (effectCount >= poolCount)
            effectCount = 0;

        puffs[effectCount].transform.position = pos.position;
        puffs[effectCount].Play();
        
        dusts[effectCount].Stop();
       dusts[effectCount].transform.position = pos.position;
        dusts[effectCount].Play();
       dusts[effectCount].transform.DOMove(endPoint.position, 1f).SetEase(Ease.OutSine).OnComplete(()=>{
            ends.Play();
        });
        

        effectCount++;
    }
}
