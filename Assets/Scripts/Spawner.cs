using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject prefab;
    [SerializeField] int blueCount;
    [SerializeField] int purpleCount;
    [SerializeField] int orangeCount;

    [SerializeField] Transform leftSpawn;
    [SerializeField] Transform rightSpawn;

    [SerializeField] float spawnRadiusMin;
    [SerializeField] float spawnRadiusMax;
    [SerializeField] DrawingLine drawingLine;
    [SerializeField] List<Material> mats;

    [Header("Debug ")]
    [SerializeField] List<Butterfly> butterflies;

    GameManager gameManager;
    GameDataSheet data;
    [SerializeField] int currentLevel;
    void Start()
    {
        gameManager = GameManager.GetManager();
        data = gameManager.GetDataSheet();
        currentLevel = gameManager.GetlevelCount();

        blueCount = data.levelDatas[currentLevel].blueSpawn;
        purpleCount = data.levelDatas[currentLevel].purpleSpawn;
        orangeCount = data.levelDatas[currentLevel].orangeSpawn;

        SpawnButterflies();
    }
    public void CheckEnclousers()
    {
        int max = butterflies.Count;
        for (int i = 0; i < max; i++)
        {
            if (butterflies[i].isActiveAndEnabled)
            {
                butterflies[i].CheckEnclouser();                
            }
        }
    }
    public void SpawnRandom(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Vector3 land;
            if (Random.Range(0f,1f) > 0.5f)
                land = leftSpawn.position;
            else
                land = rightSpawn.position;

            GameObject gg = Instantiate(prefab, land, prefab.transform.rotation);
            Vector2 rand = Random.insideUnitCircle * Random.Range(spawnRadiusMin, spawnRadiusMax);
            Vector3 pos = new Vector3(rand.x, rand.y, 0f);
            int ind = Random.Range(0, 3);
            gg.GetComponent<Butterfly>().SetButterfly(ind, drawingLine, mats[ind]);
            butterflies.Add(gg.GetComponent<Butterfly>());

            gg.transform.DOMove(transform.position + pos, 0.5f).OnComplete(()=>
            {
                gg.GetComponent<Butterfly>().Move();
            }
            );

            
        }
    }
    void SpawnButterflies()
    {
        if (blueCount + purpleCount + orangeCount < 1) 
            return;

        butterflies = new List<Butterfly>();
        for (int i = 0; i < blueCount; i++)
        {
            GameObject gg = Instantiate(prefab);
            Vector2 rand = Random.insideUnitCircle * Random.Range(spawnRadiusMin, spawnRadiusMax);
            Vector3 pos = new Vector3(rand.x, rand.y, 0f);
            gg.transform.position = transform.position + pos;
            int ind = 0;
            gg.GetComponent<Butterfly>().SetButterfly(ind, drawingLine, mats[ind]);
            butterflies.Add(gg.GetComponent<Butterfly>());
        }
        for (int i = 0; i < purpleCount; i++)
        {
            GameObject gg = Instantiate(prefab);
            Vector2 rand = Random.insideUnitCircle * Random.Range(spawnRadiusMin, spawnRadiusMax);
            Vector3 pos = new Vector3(rand.x, rand.y, 0f);
            gg.transform.position = transform.position + pos;
            int ind = 1;
            gg.GetComponent<Butterfly>().SetButterfly(ind, drawingLine, mats[ind]);
            butterflies.Add(gg.GetComponent<Butterfly>());
        }
        for (int i = 0; i < orangeCount; i++)
        {
            GameObject gg = Instantiate(prefab);
            Vector2 rand = Random.insideUnitCircle * Random.Range(spawnRadiusMin, spawnRadiusMax);
            Vector3 pos = new Vector3(rand.x, rand.y, 0f);
            gg.transform.position = transform.position + pos;
            int ind = 2;
            gg.GetComponent<Butterfly>().SetButterfly(ind, drawingLine, mats[ind]);
            butterflies.Add(gg.GetComponent<Butterfly>());
        }
    }
    
}
