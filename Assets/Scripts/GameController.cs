using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;

    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController{get{ return gameController; }}


    GameManager gameManager;
    AnalyticsController analytics;

    [SerializeField] UIController uiController;
    [SerializeField] DrawingLine drawingLine;
    [SerializeField] Spawner spawner;
    [SerializeField] EffectsController effectsController;

    public UIController GetUI { get { return uiController; } }
    public DrawingLine GetDrawingLine { get { return drawingLine; } }
    public Spawner GetSpawner { get { return spawner; } }
    public EffectsController GetEffects { get { return effectsController; } }

    [Header(" Quest targets: ")]
    [SerializeField]public int blues = 0;
    [SerializeField]public int purples = 0;
    [SerializeField]public int oranges = 0;
    GameDataSheet data;
    [SerializeField] int currentLevel;

    bool levelComplete = false;

    public void CheckCompletion(int _blue, int _pur, int _orange)
    {
        if (_blue >= blues && _pur >= purples && _orange >= oranges)
        {
            if (levelComplete)
                return;
            Debug.Log("Level complete!!");
            uiController.LevelComplete();

            analytics.LevelCompleted();
            levelComplete = true;
        }
    }


    void Start()
    {
        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();
        data = gameManager.GetDataSheet();
        analytics.LevelStarted();
        currentLevel = gameManager.GetlevelCount();

        blues = data.levelDatas[currentLevel].blueTarget;
        purples = data.levelDatas[currentLevel].purpleTarget;
        oranges = data.levelDatas[currentLevel].orangeTarget;

        Invoke("UIValues", 0.1f);
        levelComplete = false;
    }

    void UIValues()
    {
        uiController.SetTargetValues(blues, purples, oranges);
    }
}
