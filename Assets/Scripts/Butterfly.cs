using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class Butterfly : MonoBehaviour
{
    [SerializeField] float delay;
    [SerializeField] float frontGap;
    [SerializeField] Vector2 SpeedMinMax;    
    [SerializeField] ParticleSystem butterflyParticle;
    [SerializeField] Transform fbx;
    [SerializeField] SkinnedMeshRenderer ren;

    [Header("Debug: ")]
    [SerializeField] float speedPerUnit;
    [SerializeField] int typeIndex = 0;
    [SerializeField] DrawingLine dr;

    [SerializeField] float timeCount = 0;
    Vector3 globalDir;
    GameController gameController;

    readonly string CAPSULE = "capsule";
    readonly string WALL = "wall";
    void Start()
    {
        gameController = GameController.GetController;
        speedPerUnit = Random.Range(SpeedMinMax.x, SpeedMinMax.y);
        Manoeuvre(Vector3.zero);
    }

    // Update is called once per frame
    void Update()
    {
        if (timeCount >= delay )
        {
            //reset
            //recheck destinatin
            timeCount = 0f;
            PathCheck();
        }
        else
        {
            // increment time
            timeCount += Time.deltaTime;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        //reset
        //recheck destinatin
        timeCount = 0f;
        PathCheck();
    }
    public void SetButterfly(int index, DrawingLine _dr, Material _mat)
    {
        typeIndex = index;
        dr = _dr;
        ren.material = _mat;
        Manoeuvre(Vector3.zero);
    }
    public int GetIndexType
    {
        get
        {
            return typeIndex;
        }
    }
    public void CheckEnclouser()
    {
        bool foundAllFour = false;
        Vector3[] dir = { Vector3.up, Vector3.down, Vector3.left, Vector3.right };
        Ray ray;
        for (int i = 0; i < 4; i++)
        {
            ray = new Ray(transform.position, dir[i]);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 200f))
            {
                if (hit.transform.CompareTag(CAPSULE))
                {
                    foundAllFour = true;
                    //Debug.Log("enclosed in : " + dir[i]);
                    Debug.DrawLine(transform.position, hit.point, Color.blue, 4f);
                }
                else
                {
                    foundAllFour = false;
                    Debug.DrawLine(transform.position, hit.point, Color.red, 3f);
                    break;
                }
            }
        }
        if (foundAllFour)
        {
            // getcaptured
            //Debug.Log("shortlisted : " + transform.name);
            Debug.DrawLine(transform.position, Vector3.back, Color.cyan, 4f);
            StopAllMotion();
            dr.CollectButterfliesShortlist(this);            
        }
    }
    public void Catch()
    {
        gameController.GetEffects.ConsumeButterfly(transform);

        StopAllMotion();
        gameObject.SetActive(false);
    }
    public void Release()
    {
        timeCount = 0f;
        Manoeuvre(Vector3.zero);
    }
    public void Move()
    {
        StopAllMotion();
        Manoeuvre(Vector3.zero);
    }
    void PathCheck()
    {
        //Debug.Log("recheck ");
        //Ray ray = new Ray(transform.position, globalDir);
        //RaycastHit hit;
        //if (Physics.Raycast(ray, out hit, frontGap))
        //{
        //    //Debug.DrawLine(ray.origin, hit.point, Color.green, 30f);
        //    Manoeuvre();
        //}
        //------------------
        Vector3[] dir = { Vector3.up, Vector3.down, Vector3.left, Vector3.right };
        Ray ray;
        for (int i = 0; i < 4; i++)
        {
            ray = new Ray(transform.position, dir[i]);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, frontGap))
            {
                if (hit.transform.CompareTag(WALL))
                {
                    Vector3 incomingVec = hit.point - transform.position;

                    Vector3 reflectVec = Vector3.Reflect(incomingVec, hit.normal);

                    Manoeuvre(reflectVec);
                    Debug.DrawLine(transform.position, hit.point, Color.blue, 4f);
                    break;                    
                }else if (hit.transform.CompareTag(CAPSULE))
                {
                    Manoeuvre(Vector3.zero);
                    Debug.DrawLine(transform.position, hit.point, Color.green, 4f);
                    break;
                }
            }
        }
    }
    void Manoeuvre(Vector3 hitNormal)
    {
        
        //Debug.Log("Manoeuvreing ");
        Vector2 randDir = Random.insideUnitCircle;
        randDir.Normalize();
        Vector3 dir = new Vector3(randDir.x, randDir.y, 0f);
        if (hitNormal != Vector3.zero)
        {
            while (true)
            {
                randDir = Random.insideUnitCircle;
                randDir.Normalize();
                dir = new Vector3(randDir.x, randDir.y, 0f);
                //if ( Vector3.Dot(hitPoint + (dir * 2f), hitPoint)>0 )
                if ( Vector3.Dot(hitNormal, dir)>=0 )
                {
                    //Debug.Log("inside");
                    break;
                }
                else
                {
                    //Debug.Log("outside");
                }                        
            }
        }
        dir.Normalize();
        globalDir = dir;
        speedPerUnit = Random.Range(SpeedMinMax.x, SpeedMinMax.y);
        Ray ray = new Ray(transform.position, dir);
        RaycastHit hit;
        fbx.DOKill();
        fbx.DOLocalMoveX(0f, 0.01f);
        fbx.DOLocalMoveX(0.5f, 1f).SetEase(Ease.OutSine).SetLoops(-1, LoopType.Yoyo);
        if (Physics.Raycast(ray, out hit, 200f))
        {
            //transform.LookAt(hit.point);
            transform.DOMove(hit.point, (Vector3.Distance(transform.position, hit.point) / speedPerUnit)).SetEase(Ease.OutSine);
            //Debug.DrawLine(ray.origin, hit.point, Color.blue, 30f);
            Quaternion qq  = Quaternion.LookRotation((hit.point - transform.position), -dr.transform.forward);
            transform.DORotateQuaternion(qq, 0.4f);

            //fbx.DOLocalMoveX(0.5f, 0.3f).SetEase(Ease.OutSine).SetLoops(-1, LoopType.Yoyo);
        }

        CheckAngle();
    }

    
    void StopAllMotion()
    {
        transform.DOKill();
    }
    void CheckAngle()
    {
        return;

        Vector3 dd = globalDir * 2;
        float angle = Vector3.Angle(globalDir, Vector3.up);
        var par = butterflyParticle.main;
        if (dd.x >= 0)
        {
            par.startRotation = angle * Mathf.Deg2Rad;
        }
        else
        {
            par.startRotation = (360f - angle)* Mathf.Deg2Rad;
        }
         
    }

    
}
