﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;

    [SerializeField] bool tutorialShown = false;
    [SerializeField] int gameplaySceneNumber;

    [Header("For Debugging")]
    public int levelcount = 0;
    public int totalScenes = 0;
    public int lastLevelPlayed = 1;
    public bool isGameOpeningLevel = true;
    [SerializeField] DataManager dataManager;
    [SerializeField] GamePlayer gamePlayer;
    [SerializeField] GameDataSheet dataSheet;

    

    private void Awake()
    {
        gameManager = this;
        DontDestroyOnLoad(this.gameObject);
        dataManager = GetComponent<DataManager>();
        gamePlayer = new GamePlayer();
    }
    void Start()
    {
        totalScenes = dataSheet.levelDatas.Count;
        
        gamePlayer = dataManager.GetGamePlayer;
        if (gamePlayer.handTutorialShown)
        {
            TutorialShown();
        }
        //Load last played level
        lastLevelPlayed = gamePlayer.lastPlayedLevel;
        levelcount = gamePlayer.lastPlayedLevel;
        //Debug.Log("level count : " + levelcount);
        SceneManager.LoadScene(gameplaySceneNumber);
    }

    public static GameManager GetManager()
    {
        return gameManager;
    }
    public void GotoNextStage()
    {

        //if (levelcount < dataSheet.levelDatas.Count - 1)  
        if (levelcount < totalScenes - 1)  
            levelcount++;
        else
            levelcount = 0;

        SceneManager.LoadScene(gameplaySceneNumber);
        OpeningLevelComplete();
    }
    public void ResetStage()
    {
        SceneManager.LoadScene(gameplaySceneNumber);
    }
    public int GetlevelCount()
    {
        return levelcount;
    }
    public bool TutorialAlreadySeen()
    {
        return tutorialShown;
    }
    public void TutorialShown()
    {
        tutorialShown = true;
        dataManager.GetGamePlayer.handTutorialShown = true;
    }
    public DataManager GetDataManager()
    {
        return dataManager;
    }
    public GameDataSheet GetDataSheet()
    {
        return dataSheet;
    }
    public bool IsGameOpeningLevel()
    {
        return isGameOpeningLevel;
    }
    public void OpeningLevelComplete()
    {
        isGameOpeningLevel = false;
    }
}
