﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GameDataSheet", order = 1)]
public class GameDataSheet : ScriptableObject
{
    public List<LevelData> levelDatas;

    private void Awake()
    {
               
    }

    
}


[System.Serializable]
public struct LevelData
{
    public int blueSpawn;
    public int purpleSpawn;
    public int orangeSpawn;

    public int blueTarget;
    public int purpleTarget;
    public int orangeTarget;
}