using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DrawingLine : MonoBehaviour
{
    [SerializeField] Camera mainCam;
    [SerializeField] Line line;
    [SerializeField] LineRenderer redLine;
    [SerializeField] LayerMask WALL;
    [SerializeField] Spawner spawner;

    [Header("Debug: ")]
    [SerializeField] int blue = 0;
    [SerializeField] int purple = 0;
    [SerializeField] int orange = 0;
    [SerializeField] int currentTypeIndex = 0;
    [SerializeField] List<Butterfly> catchShortList;
    [SerializeField] List<Butterfly> butterfliesCaught;
    [SerializeField] List<Vector3> points;

    WaitForSeconds WAITONE = new WaitForSeconds(0.1f);
    GameController gameController;
    void Start()
    {
        gameController = GameController.GetController;

        catchShortList = new List<Butterfly>();
        butterfliesCaught = new List<Butterfly>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            BiginDraw();
        }
        if (Input.GetMouseButton(0))
        {
            Draw();
        }
        if (Input.GetMouseButtonUp(0))
        {
            EndDraw();
        }
    }
    public void CollectButterfliesShortlist(Butterfly bt)
    {
        catchShortList.Add(bt);
    }
    IEnumerator AnalysisRoutine()
    {
        yield return WAITONE;

        bool allSame = false;
        int max = catchShortList.Count;
        if (max > 0)
        {
            currentTypeIndex = catchShortList[0].GetIndexType;
            for (int i = 0; i < max; i++)
            {
                if (catchShortList[i].GetIndexType == currentTypeIndex)
                {
                    allSame = true;
                }
                else
                {
                    allSame = false;
                    
                    break;
                }
            }
            if (allSame)
            {
                Debug.Log("Same bro!!");
                for (int i = 0; i < max; i++)
                {
                    catchShortList[i].Catch();
                    butterfliesCaught.Add(catchShortList[i]);
                    CollectionCheck(currentTypeIndex);
                }
                spawner.SpawnRandom(max);
                gameController.GetUI.ShowComboText(max);
            }
            else
            {
                Debug.Log("We are not same bro!!");
                for (int i = 0; i < max; i++)
                {
                    catchShortList[i].Release();
                }
                int tt = points.Count;
                //for (int i = 0; i < tt; i++)
                //{
                //    redLine.AddPoint(points[i]);
                //}
                redLine.positionCount = tt;
                redLine.SetPositions(points.ToArray());
                StartCoroutine(RedLineRoutine());
            }
        }

        
    }
    void Analysis()
    {
        StartCoroutine(AnalysisRoutine());        
    }
    void BiginDraw() {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 200f))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.white, 30f);
            line.FirstPoint(hit.point);
        }
        catchShortList = new List<Butterfly>();
    }
    void Draw() {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 200f,WALL))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.green);
            line.AddPoint(hit.point);
        }
    }
    void EndDraw() {
        spawner.CheckEnclousers();
        line.LastPoint();
        points = new List<Vector3>();
        points = line.GetPoints();
        Analysis();
    }

    void CollectionCheck(int _type)
    {
        switch (_type)
        {
            case 0:
                blue++;
                break;
            case 1:
                purple++;
                break;
            case 2:
                orange++;
                break;

            default:
                break;
        }

        gameController.GetUI.UpdateUI(blue, purple, orange);
    }
    IEnumerator RedLineRoutine()
    {
        redLine.startWidth = 0.1f;
        redLine.endWidth = 0.1f;
        while (redLine.startWidth < 0.5f)
        {
            redLine.startWidth += Time.deltaTime;
            redLine.endWidth += Time.deltaTime;
            yield return null;
        }
        yield return null;
        redLine.startWidth = 0.5f;
        redLine.endWidth = 0.5f;
        while (redLine.startWidth > 0.1f)
        {
            redLine.startWidth -= Time.deltaTime;
            redLine.endWidth -= Time.deltaTime;
            yield return null;
        }
        yield return null;
        redLine.positionCount = 0;
    }
}
