using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{

    [SerializeField] GameObject capsulePrefab;
    [Header("Debug : ")]
    [SerializeField] LineRenderer ln;
    [SerializeField] float widthLn;
    [SerializeField] float minDistance;
    [SerializeField] float minDistanceEndPoint;
    [SerializeField] float capsuleRadius;
    [SerializeField] List<Vector3> points;
    [SerializeField] List<GameObject> objects;
    [SerializeField] List<CapsuleCollider> colliders;

    
    
    private void Awake()
    {
        ln = GetComponent<LineRenderer>();
    }
    void Start()
    {
        points = new List<Vector3>();
        objects = new List<GameObject>();
        colliders = new List<CapsuleCollider>();
        ResetLine();
        ln.startWidth = widthLn;
        ln.endWidth = widthLn;
    }
    public void FirstPoint(Vector3 newPoint)
    {
        ln.positionCount = 1;
        ln.SetPosition(0, newPoint);
    }
    public void AddPoint(Vector3 newPoint)
    {
        float dis = Vector3.Distance(newPoint, GetLastPoint());
        //Debug.Log("distance: " + dis);
        if (ln.positionCount >= 1 && dis > minDistance )
        {
            //Debug.Log("adding point");
            points.Add(newPoint);
            Vector3 midPoint = (newPoint + GetLastPoint()) / 2f;
            float height = Vector3.Distance(newPoint, GetLastPoint());
            //GameObject gg = Instantiate(capsulePrefab, newPoint, Quaternion.identity);
            GameObject gg = Instantiate(capsulePrefab, midPoint, Quaternion.identity, transform);
            objects.Add(gg);
            colliders.Add(gg.GetComponent<CapsuleCollider>());
            gg.GetComponent<CapsuleCollider>().radius = capsuleRadius;
            gg.GetComponent<CapsuleCollider>().height = height;
            Vector3 dir = (newPoint - GetLastPoint()).normalized;
            gg.transform.up = dir;

            ln.positionCount += 1;
            ln.SetPosition(ln.positionCount -1 , newPoint);
        }
    }
    public void LastPoint()
    {
        if (Vector3.Distance(ln.GetPosition(0), GetLastPoint()) < minDistanceEndPoint) 
        {
            //Debug.Log("lastPoint!!");
            points.Add(ln.GetPosition(0));
            Vector3 midPoint = (ln.GetPosition(0) + GetLastPoint()) / 2f;
            float height = Vector3.Distance(ln.GetPosition(0), GetLastPoint());
            //GameObject gg = Instantiate(capsulePrefab, newPoint, Quaternion.identity);
            GameObject gg = Instantiate(capsulePrefab, midPoint, Quaternion.identity, transform);
            objects.Add(gg);
            colliders.Add(gg.GetComponent<CapsuleCollider>());
            gg.GetComponent<CapsuleCollider>().radius = capsuleRadius;
            gg.GetComponent<CapsuleCollider>().height = height;
            Vector3 dir = (ln.GetPosition(0) - GetLastPoint()).normalized;
            gg.transform.up = dir;

            ln.positionCount += 1;
            ln.SetPosition(ln.positionCount - 1, ln.GetPosition(0));
            //Debug.Log("adding point");
        }
        //checked here for butter flies
        ResetLine();
    }
    public void ResetLine()
    {
        ln.positionCount = 0;
        int maxObj = objects.Count;
        for (int i = 0; i < maxObj; i++)
        {
            objects[i].SetActive(false);
        }
        // rest colliders
    }
    public List<Vector3> GetPoints()
    {
        List<Vector3> pp = points;
        points = new List<Vector3>();
        return pp;
    }
    Vector3 GetLastPoint()
    {
        return ln.GetPosition(ln.positionCount - 1);
    }
    
}
